#!/usr/bin/env python3
import sys
import os
import os.path
import pika
import time
import datetime
import json
import threading
import dateutil.parser

from statistics import mean
from influxdb import InfluxDBClient

class EventsListener(threading.Thread):
    def __init__(self, callback=None):
        self.callback = callback
        self.connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
        self.channel = self.connection.channel()

        result = self.channel.queue_declare(queue='', exclusive=True)
        queue_name = result.method.queue

        self.channel.queue_bind(exchange='events', queue=queue_name)
        self.channel.basic_consume(queue=queue_name, on_message_callback=self._callback, auto_ack=True)

        super().__init__()

    def stop(self):
        self.channel.stop_consuming()

    def run(self):
        self.channel.start_consuming()
    
    def _callback(self, ch, method, properties, body):
        event = json.loads(body.decode("utf-8"))
        if self.callback is not None:
            self.callback(event)


class ContinuousMonitor(threading.Thread):
    def __init__(self, callback=None):
        self._stop = False
        self.callback = callback
        self.client = InfluxDBClient(host="gordrakk.esrf.fr", port=8086, database="beegfs")
        self.values = []

        super().__init__()

    def stop(self):
        self._stop = True

    def run(self):
        while not self._stop:
            result = self.client.query('SELECT sum("write_bps") AS "write_bps" FROM (SELECT derivative(mean("write_sectors"), 5s)*512/5 AS "write_bps" FROM "beegfs"."autogen"."iostat" WHERE time > now() - 1m AND time < now() GROUP BY time(10s), "host", "disk" FILL(linear)) GROUP BY time(10s);')
            self.values = list(map(lambda x:x.get('write_bps'), sorted(result.get_points("iostat"), key=lambda x:dateutil.parser.parse(x['time']), reverse=True)[1:-1]))

            if self.callback is not None:
                self.callback(self)

            time.sleep(5)

    @property
    def mean(self):
        return mean(self.values)

    @property
    def current(self):
        return self.values[0]

    def __str__(self):
        return "Current: {:.2f} Gbps\tMean: {:.2f} Gbps".format(self.current / 1024**3, self.mean / 1024**3)


class AdaptiveCopier(object):
    def __init__(self, block_size=8 * 1024**2):
        self._stop = False
        self.max_mbps = -1

        self.data_avail = threading.Condition()
        self.buffer_full = threading.Condition()

        self.block_size = block_size
        self.buffer_block_count = 100
        self.buffer = bytearray(self.block_size * self.buffer_block_count)
        self.last_block_size = 0

        self.reader_position = 0
        self.writer_position = 0
        self.reading = False

    def stop(self):
        self._stop = True
        with self.buffer_full:
            self.buffer_full.notify()
        with self.data_avail:
            self.data_avail.notify()

    ###
    # KNOWN TO BE SLOW!!!
    # The principe here is to be able to interrupt the transfert
    # With sendfile, it is impossible
    #
    def copy_one(self, source, target):
        print(" [x] Start to copy %s" % source)

        os.makedirs(os.path.dirname(target), exist_ok=True)

        self.reading = True
        self.reader_position = 0
        self.writer_position = 0

        reader = threading.Thread(target = self.reader_thread, args = (source,))
        reader.start()

        writer = threading.Thread(target = self.writer_thread, args = (target,))
        writer.start()

        reader.join()
        writer.join()

        source_stat = os.stat(source)
        os.chown(target, source_stat.st_uid, source_stat.st_gid)
        os.chmod(target, source_stat.st_mode)
        os.utime(target, (source_stat.st_atime, source_stat.st_mtime))
        #print(" [x] Finished %s" % source)

    def reader_thread(self, source):
        global_start_time = datetime.datetime.now()

        with open(source, 'rb') as f_source:
            while not self._stop:
                with self.buffer_full:
                    if self.reader_position - self.writer_position > self.buffer_block_count:
                        self.buffer_full.wait()

                reader_block = self.reader_position % self.buffer_block_count
                start_position = reader_block * self.block_size
                reader_window = memoryview(self.buffer)[start_position:start_position + self.block_size]

                start_time = datetime.datetime.now()
                read_count = f_source.readinto(reader_window)
                duration = datetime.datetime.now() - start_time
                speed_compensation = (read_count / (self.max_mbps * 1024**2)) - duration.total_seconds()

                if read_count < self.block_size:
                    self.last_block_size = read_count
                    if self.max_mbps > 0 and speed_compensation > 0:
                        time.sleep(speed_compensation)
                    sys.stdout.write("!")
                    break

                with self.data_avail:
                    self.reader_position += 1
                    self.data_avail.notify()

                if self.max_mbps > 0 and speed_compensation > 0:
                    time.sleep(speed_compensation)
                    sys.stdout.write("s")
                else:
                    sys.stdout.write(".")
                sys.stdout.flush()

        total_duration = datetime.datetime.now() - global_start_time
        total_mb = (self.reader_position * self.block_size + self.last_block_size) / (1024**2)
        sys.stdout.write(" {:.2f} Mbps\n".format(total_mb / total_duration.total_seconds()))
        sys.stdout.flush()

        with self.data_avail:
            self.reading = False
            self.data_avail.notify()

    def writer_thread(self, target):
        with open(target, "wb+") as f_target:
            while not self._stop:
                with self.data_avail:
                    if not self.reading and self.writer_position > self.reader_position:
                        break

                    if self.reading and self.reader_position <= self.writer_position:
                        self.data_avail.wait()

                writer_block = self.writer_position % self.buffer_block_count

                start_position = writer_block * self.block_size
                if not self.reading and self.writer_position >= self.reader_position:
                    block_size = self.last_block_size
                else:
                    block_size = self.block_size

                writer_window = memoryview(self.buffer)[start_position:start_position + block_size]
                f_target.write(writer_window)

                with self.buffer_full:
                    self.writer_position += 1
                    self.buffer_full.notify()


class OnCommit(object):
    def __init__(self, copier):
        self.copier = copier

    def __call__(self, ch, method, properties, body):
        try:
            event = json.loads(body.decode("utf-8"))
            path = event.get("path")
            source_path = os.path.join("/mnt/beegfs", path[1:])
            target_path = os.path.join("/mnt/local", path[1:])

            if os.path.exists(source_path):
                try:
                    copier.copy_one(source_path, target_path)
                    #os.unlink(source_path)
                except KeyboardInterrupt:
                    copier.stop()
                    raise

            ch.basic_ack(delivery_tag = method.delivery_tag)
        except KeyboardInterrupt:
            raise
        except:
            #TODO: report errors
            ch.basic_nack(delivery_tag = method.delivery_tag)


class Cooldown(threading.Thread):
    def __init__(self, callback=None, duration=10):
        self._stop = False
        self.callback = callback
        self.duration = duration
        self.watchdog = 0

        super().__init__()

    def stop(self):
        self._stop = True

    def run(self):
        while not self._stop:
            time.sleep(1)

            if self.watchdog > 0:
                self.watchdog -= 1

            if self.callback is not None and self.watchdog <= 0:
                self.callback()

    def set(self):
        self.watchdog = self.duration

    @property
    def is_set(self):
        return self.watchdog > 0

MAX_MBPS = 8

if __name__ == "__main__":
    copier = AdaptiveCopier()
    on_commit = OnCommit(copier)
    cooldown = Cooldown()
    cooldown.start()

    def mon_callback(stats):
        copier.max_mbps = MAX_MBPS if cooldown.is_set or stats.current > 100 * 1024**2 else -1
    continuous_monitor = ContinuousMonitor(mon_callback)

    def evt_callback(event):
        if event.get("type") in [ "FLUSH", "TRUNCATE", "CREATE", "CLOSE_WRITE" ]:
            copier.max_mbps = MAX_MBPS
            cooldown.set()
    events_listener = EventsListener(evt_callback)

    connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
    channel = connection.channel()
    channel.basic_qos(prefetch_count=1)

    channel.queue_declare(queue='commits')
    channel.basic_consume(queue='commits', on_message_callback=on_commit, auto_ack=False)

    try:
        continuous_monitor.start()
        events_listener.start()
        channel.start_consuming()
    except KeyboardInterrupt:
        channel.stop_consuming()

    print(" [!] Stopping...")
    connection.close()
    copier.stop()
    cooldown.stop()
    continuous_monitor.stop()
    events_listener.stop()
